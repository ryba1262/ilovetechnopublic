import { NgModule } from '@angular/core';
import {AutoCompleteModule} from 'primeng/autocomplete';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SortArtistsPipe } from '../pipes/sort-artists.pipe';
import { TranslateModule } from '@ngx-translate/core';
import { DialogModule } from 'primeng/dialog';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';


import {SpinnerModule} from 'primeng/spinner';
import {SliderModule} from 'primeng/slider';





@NgModule ({
  declarations: [
  SortArtistsPipe,

  ],
  imports: [
    FormsModule,
    AutoCompleteModule,
    DialogModule,
    SliderModule,

    SpinnerModule,
    ReactiveFormsModule,
    LeafletModule.forRoot(),
  ],

  exports: [AutoCompleteModule, FormsModule, SortArtistsPipe
    , TranslateModule, DialogModule, SliderModule
    , LeafletModule ]
})
export class SharedModule {}
