import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { AuthService } from './services/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { HttpService } from './services/http.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  name: any;
  items: MenuItem[];
  constructor(
      public _authService: AuthService
    , private translateService: TranslateService
    , private messageService: MessageService
    , private _router: Router
    ) {
   }

  ngOnInit() {
    this._authService.checkTokenTime();
    this.translateService.addLangs(['eng', 'pl']);
    const browser = this.translateService.getBrowserLang();
     if (browser === 'pl') {
        this.translateService.setDefaultLang(browser); } else {
          this.translateService.setDefaultLang('eng');

        }


    this.items = [
      {
        label: 'Kluby',
        automationId: '*NgIf="visible = 2;"',
        items: [
          [
            {
              label: 'Podstrona 1',
              items: [{ label: 'Podstrona 1.1' }, { label: 'Podstrona 1.2' }]
            },
            {
              label: 'Podstrona 2',
              items: [{ label: 'Podstrona 2.1' }, { label: 'Podstrona 2.2' }]
            }
          ],
          [
            {
              label: 'Podstrona 3',
              items: [{ label: 'Podstrona 3.1' }, { label: 'Podstrona 3.2' }]
            },
            {
              label: 'Podstrona 4',
              items: [{ label: 'Podstrona 4.1' }, { label: 'Podstrona 4.2' }]
            }
          ]
        ]
      }
    ];
  }

  switchLanguage(language: string) {
    this.translateService.use(language);
  }

  addSingle() {
    const token: string = this._authService.getToken();
    const timestamp = Math.floor(Date.now() / 1000) + 60;
    const start = token.indexOf('.');
    const end = token.lastIndexOf('.');
    const decode = JSON.parse(atob(token.slice(start + 1, end))).exp;
    console.log('timestampNow: ' + timestamp);
    console.log('token: ' + decode);
  }

  logout() {
    this._authService.logoutUser();
    this._router.navigate(['/login']);
  }
}
