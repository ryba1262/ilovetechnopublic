import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { SearchComponent } from './components/search/search.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [

  { path: '', redirectTo: '/szukam', pathMatch: 'full' },
  { path: 'miasta/:id', loadChildren: './components/events/events.module#EventsModule' },
  { path: 'kluby/:id', loadChildren: './components/events/events.module#EventsModule' },
  { path: 'szukam', component: SearchComponent },
  { path: 'user', loadChildren: './components/user-panel/user-panel.module#UserPanelModule', canActivate: [AuthGuard] },

  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
