import { Injectable } from '@angular/core';
import { Cities } from '../models/cities';
import { Observable } from 'rxjs';
import { HttpClient} from '@angular/common/http';
import { Artists } from '../models/artists';
import { Places } from '../models/places';
import { Places2 } from '../models/places2';

import { EventID } from '../models/event2';
import { Event$ } from '../models/event';
import { urlApi } from '../models/urlApi';
import { MessageService } from 'primeng/api';
import { MyEvents } from '../models/myEvents';
import { RouterLink, Router } from '@angular/router';
import { MyLists } from '../models/myLists';
import { List } from '../models/list';
import { first } from 'rxjs/operators';
import {User} from '../models/user';


@Injectable()
export class HttpService {



  constructor(private http: HttpClient, private messageService: MessageService, private router: Router) {

   }



searchCityOrPlace(par: urlApi): Observable<Array<Places2>> {
  return this.http.get<Array<Places2>>(urlApi.searchCityorPplace + par);
 }

 searchPlaces(par): Observable<Array<Places>> {
  return this.http.get<Array<Places>>(urlApi.searchPlace + par);
 }

 searchArtists(par: urlApi): Observable<Array<Artists>> {
  return this.http.get<Array<Artists>>(urlApi.searchArtist + par);
 }

getEvent(id: number) {
  return this.http.get<Event$>(urlApi.getEvent + id);
 }



getCityEvent(place: any, days: number): Observable<Array<EventID>> {
  return this.http.get<Array<EventID>>(urlApi.getCityEvents + place + '/' + days);
}

getPlaceEvent(place: any, days: number): Observable<Array<EventID>> {
  return this.http.get<Array<EventID>>(urlApi.getPlaceEvents + place + '/' + days);
}
// getCityIDEvent(id: any) {
//   return this.http.get<Event>
// }

sendEvent(event, img: FormData) {
  return this.http.post(urlApi.postEvent, (event as JSON) )
  .subscribe(
    res => {
      if (img != null) {
        this.sendImg( res , img);
      } else {
        this.addSingle();
      }
    }
  );
}


sendImg( id: any, file: FormData) {
  return this.http.post('https://technoweekapi20190128024719.azurewebsites.net/api/Event/UploadEventCover/' + id.eventID, file)
  .subscribe(
    res => {
     console.log(res);
     this.addSingle();
    }
   );
}


myEvents(): Observable<Array<MyEvents>> {
  return this.http.get<Array<MyEvents>>(urlApi.myEvents);
}

myLists(): Observable<Array<MyLists>> {
  return this.http.get<Array<MyLists>>(urlApi.myLists);
}

addSingle() {
  this.messageService.add({ severity: 'success', summary: 'Sukces', detail: 'Event został dodany pomyślnie' });
}

publishEvent(id: number) {
  return this.http.post(urlApi.publishEvent + id, null);
}

createList(list: List) {
  return this.http.post(urlApi.createList, list)
  .pipe(first()).subscribe(
    res => {
      this.router.navigate(['user/mylists']);
      this.messageService.add({ severity: 'success', summary: 'Sukces', detail: 'Lista została dodana pomyślnie' });
    }
  );
}

findUser(user)  {
  return this.http.post<Array<any>>(urlApi.findUser, user);
}

getEventUsers(id: number): Observable<Array<User>> {
  return this.http.get<Array<User>>(urlApi.getEventUsers + id);
}

removeUserList(eventID: number, userID: number) {
  return this.http.post(urlApi.removeUserList + eventID + '/' + userID, null);
}

addUserToList(event: number, user: number) {
  return this.http.post(urlApi.addUserToList + '/' + event + '/' + user, null);
}

}

