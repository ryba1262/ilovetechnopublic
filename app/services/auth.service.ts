import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { urlApi } from '../models/urlApi';
import { CookieService } from 'ngx-cookie-service';
import { first } from 'rxjs/operators';
import { ReqHeader } from '../models/header';


@Injectable()
export class AuthService {

  constructor(private http: HttpClient
    , private _router: Router
    , private cookieService: CookieService
    ) { }

  registerUser(login: string, password: string, email: string) {
    return this.http.post<any>(urlApi.register, { login, password, email });
  }

  loginUser(username: string, password: string) {
    return this.http.post<any>(urlApi.login, { username, password });
    // {responseType: 'text' as 'json'}
  }

  logIn(res: any) {
    const mapped =  Object.keys(res).map(e => res[e]);
    const bearer = mapped[0];
    const renew = mapped[1];
    const keys = { bearer: bearer, renew: renew };
    // console.log(keys);
    this.cookieService.set('token', bearer );
    this.cookieService.set('renew', renew );
    this.checkTokenTime();
  }

  renewToken() {
    const httpOptions = {
      headers: new HttpHeaders({
        'renewToken': this.getRenew()
      })
    };
    return this.http.post( urlApi.renewToken, null, httpOptions )
    .pipe(first()).subscribe(
      res => {
        this.logIn(res);
        console.log(res);
        // this.logIn(res);
      }
    );
  }
checkTokenTime() {
  const token: string = this.getToken();
    const nowTime = Math.floor(Date.now() / 1000) + 60;
    const start = token.indexOf('.');
    const end = token.lastIndexOf('.');
    const decode = JSON.parse(atob(token.slice(start + 1, end))).exp;
    console.log('różnica: ' + (decode - nowTime) * 1000);
    console.log('teraz: ' + nowTime);
    console.log('decode: ' + decode);

     if ( token.length > 0 ) {
       if (decode >= nowTime) {
         setTimeout( () => {
           console.log('Sprawdzam!');
           this.renewToken();
         } , (decode - nowTime) * 1000);
       } else {
          this.logoutUser();
       }
     } return false;
}

  logoutUser() {
    this.cookieService.deleteAll();
  }

  getToken() {
    return  this.cookieService.get('token');
  }

  getRenew() {
    return  this.cookieService.get('renew');
  }

  checkToken(): boolean {
    return this.cookieService.check('token');
  }

  public loggedIn() {
    return !!this.cookieService.get('token');
  }

}
