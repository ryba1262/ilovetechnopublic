import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';




@Injectable({
  providedIn: 'root'
})
export class CityService {

gpsPositionObs = new BehaviorSubject<any[]>(undefined);


  constructor() {
   }

  toGPS(a: number, b: number) {
      const c = [];
      c.push(a, b);
      const g = this.gpsPositionObs.next(c);
      console.log(g);
  }


   getGpsObs(): Observable<any[]> {
    return this.gpsPositionObs.asObservable();
    }

    // etCityObs(): Observable<Array<number>> {
    //   return this.gb.asObservable();
    //   }

}
