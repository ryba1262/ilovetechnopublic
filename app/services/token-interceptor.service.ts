import { Injectable, Injector } from '@angular/core';
import { AuthService } from './auth.service';
import { HttpInterceptor, HttpEvent, HttpResponse, HttpHeaders } from '@angular/common/http';
import { LoaderService } from './loader.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { urlApi } from '../models/urlApi';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor(private injector: Injector, private loaderService: LoaderService) {}
  intercept(req, next):
  Observable<HttpEvent<any>> {
      this.showLoader();
    const authService = this.injector.get(AuthService);
      const tokenizedReq = req.clone(
        {
          headers: req.headers.set('Authorization', 'Bearer ' + authService.getToken())
        }
      );
      return next.handle(tokenizedReq).pipe(tap((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          this.onEnd();
        }
      },
        (err: any) => {
          this.onEnd();
      }));


  }
  private onEnd(): void {
    this.hideLoader();
  }
  private showLoader(): void {
    this.loaderService.show();
  }
  private hideLoader(): void {
    this.loaderService.hide();
  }
  }
