import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Places } from '../models/places';
import { DateEvent } from '../models/dateEvent';
import { Links } from '../models/links';
import { HttpService } from './http.service';
import { HttpClient} from '@angular/common/http';
import { urlApi } from '../models/urlApi';
import { Event$ } from '../models/event';
import { Artists } from '../models/artists';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class AdditionService {
  placeList = new BehaviorSubject<Array<Places>>([]);
  artistsList = new BehaviorSubject<Array<any>>([]);
  currentOrderArt = 0;
  linkList = new BehaviorSubject<Array<Links>>([]);
  currentOrderLink = 0;
  calendar = new BehaviorSubject<DateEvent>(null);
  imgBase64 = new BehaviorSubject<any>(null);
  eventDescription = new BehaviorSubject<string>(null);
  eventTitle = new BehaviorSubject<string>(null);
  finishEvent: Date;
  currentPageObs = new BehaviorSubject<number>(0);
  eventID = new BehaviorSubject<number>(null);
  imgURL = new BehaviorSubject<string>(null);
  constructor( private htt: HttpService, private http: HttpClient, private router: Router, private messageService: MessageService) { }

  ruskiDebag() {
    const start = this.calendar.getValue().start;
    // const d = start.getDate();
    // const m = start.getMonth;
    // const y = start.getFullYear;
    // const t = start.getTime;
    console.log(start.getUTCDate());
  }


  addArtist(artist: any): Observable<Array<any>> {
    if (artist.name === undefined) {
      this.currentOrderArt++;
      const art = {artistID: null, name: artist, order: this.currentOrderArt, isLive: false};
      const art2 = this.artistsList.getValue();
      art2.push(art);
      this.artistsList.next(art2);
    } else {
      this.currentOrderArt++;
      const a: number = artist.artistID;
      const art = {artistID: a, name: artist.name, order: this.currentOrderArt, isLive: false};
      const art2 = this.artistsList.getValue();
      art2.push(art);
    this.artistsList.next(art2);
    }
    return(this.artistsList);
  }


  addPlace(place: Places): Observable<Array<Places>> {
    const pla = this.placeList.getValue();
    pla.push(place);
    this.placeList.next(pla);
    return(this.placeList).asObservable();
  }

    removePlace(pl: Array<Places>) {
    this.placeList.next(pl);
  }

  addLink(link: string, type: number): Observable<Array<Links>> {
    this.currentOrderLink++;
    const li = { link: link, type: type, order: this.currentOrderLink  }
    const l = this.linkList.getValue();
    l.push(li);
    this.linkList.next(l);
    return this.linkList;
  }

  removeLink(link: Array<Links>) {
    this.linkList.next(link);
  }

  removeArt(art: Array<any>) {
    this.artistsList.next(art);
  }

  addCalendar(data: DateEvent) {
    this.calendar.next(data);
    if (data.finish !== undefined) {
      this.finishEvent = data.finish;
    } else {
      this.finishEvent = null;
    }
  }

  removeCalendar() {
    this.calendar.next(null);
  }

  removeFinishDate() {
    const date = this.calendar.getValue();
    date.finish = null;
    this.calendar.next(date);
  }

  addImage(img: any): Observable<any> {
    this.imgBase64.next(img);
    return(this.imgBase64).asObservable();
  }

  removeImage(): Observable<any> {
    const img = null;
    this.imgBase64.next(img);
    return(this.imgBase64).asObservable();
  }

  addTitle(t: string): Observable<string> {
    this.eventTitle.next(t);
    return this.eventTitle;
  }

  addDescription(t: string): Observable<string> {
    this.eventDescription.next(t);
    return this.eventDescription;
  }

  constEvent() {
    const event = {
      name: this.eventTitle.getValue(),
      description: this.eventDescription.getValue(),
      placeID: this.placeList.getValue().map( p => p.placeID )[0],
      draft: true,
      dateStart: this.calendar.getValue().start,
      dateEnd: this.calendar.getValue().finish,
      artist: this.artistsList.getValue(),
      link: this.linkList.getValue()
     };
     return event;
  }

  constImg() {
    if (this.imgBase64.getValue() !== null ){
      const img = new FormData();
      img.append('file', this.imgBase64.getValue());
      return img;
    } else {
      return null;
    }
  }

sendEvent() {
  const event = this.constEvent();
  const img = this.constImg();
  let urlApi$;
  if( this.eventID.getValue() === null ) {
    urlApi$ = urlApi.postEvent;
  } else {
    urlApi$ = urlApi.editEvent + this.eventID.getValue();
  }
   return this.http.post(urlApi$, (event) )
   .subscribe(
     res => {
       if (img !== null) {
         this.sendImg( res , img);
       } else {
         this.clearEvent();
         this.router.navigate(['/user/myevents']);
         this.toast();
       }
     }
   );
}

sendImg( id: any, file: FormData) {
  return this.http.post(urlApi.sendImg + id.eventID, file)
  .subscribe(
    res => {
     console.log(res);
     this.clearEvent();
     this.router.navigate(['/user/myevents']);
     this.toast();
    }
   );
}

editEvent(p: number) {
  this.http.get<Event$>(urlApi.getEvent + p)
  .subscribe(
    res => {
      console.log(res.eventID);
      console.log(res);
      this.clearEvent();
      this.eventID.next(res.eventID);
      const place = { placeID: res.placeID, name: res.placeName, adress: res.placeAddress, longitude: res.placeLongitude, latitude: res.placeLatitude };
      const arr = [];
      arr.push(place);
      this.placeList.next(arr);
      this.artistsList.next(res.artists);
      this.linkList.next(res.links);
      const cal = { start: res.dateTimeStart, finish: res.dateTimeEnd };
      this.calendar.next(cal);
      this.eventTitle.next(res.name);
      this.eventDescription.next(res.description);
      this.imgURL.next(res.imgURL);
      this.router.navigate(['user/editevent']);
    }
  );
}

navi() {
this.router.navigate(['user/editevent']);
}

  clearEvent() {
    this.placeList.next([]);
    this.artistsList.next([]);
    this.linkList.next([]);
    this.calendar.next(null);
    this.imgBase64.next(null);
    this.eventTitle.next(null);
    this.eventDescription.next(null);
    this.currentPageObs.next(0);
    this.currentOrderArt = 0;
    this.currentOrderLink = 0;
    this.currentPageObs.next(0);
    this.eventID.next(null);
    this.imgURL.next(null);
  }

  toast() {
    this.messageService.add({ severity: 'success', summary: 'Sukces', detail: 'Event został dodany pomyślnie' });
  }

   changePage(delta: number) {
     let current = this.currentPageObs.getValue();
     current += delta;
    this.currentPageObs.next(current);
}



}
