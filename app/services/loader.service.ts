import { Injectable } from '@angular/core';
import { LoaderState } from '../components/loader/loader.model';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  onShow = true;
  private loaderSubject = new Subject<LoaderState>();
  loaderState = this.loaderSubject.asObservable();
  private errorSubject = new Subject<Error>();
  errorState = this.errorSubject.asObservable();
  constructor() { }

  toggleShow(show) {
    this.onShow = show;
  }

  show() {
    if (this.onShow === true) {
      this.loaderSubject.next(<LoaderState>{ show: true });
      console.log(this.loaderState);
    }

  }
  hide() {
    this.loaderSubject.next(<LoaderState>{ show: false });
  }
  showError(s: Error) {
    console.log(s);
    this.errorSubject.next(s);
  }

  deleteError() {
    this.errorSubject._isScalar = null;
    this.errorState = null;
  }
}
