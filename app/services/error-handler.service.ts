import { Injectable, ErrorHandler, Injector } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { LoaderService } from './loader.service';
@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService implements ErrorHandler {

   errorSubject = new BehaviorSubject<any>(undefined);
  constructor(private injector: Injector ) { }
  handleError(error: Error) {
    const printService = this.injector.get(LoaderService);
    printService.showError(error);
  }
}
