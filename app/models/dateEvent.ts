export interface DateEvent {
  start: Date;
  finish: Date;
}
