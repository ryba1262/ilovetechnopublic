export interface MyEvents {
eventID:	number;
name:	string;
placeName: string;
dateStart: Date;
dateEnd: Date;
draft:	boolean;
status:	number;
}
