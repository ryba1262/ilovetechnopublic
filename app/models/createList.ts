export interface CreateList {
  eventID: number;
  name: string;
  maxGuests: number;
  price: string;
}
