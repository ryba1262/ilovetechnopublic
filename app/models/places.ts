export interface Places {
  placeID: number;
    name: string;
    adress: string;
    longitude: number;
    latitude: number;
    cityID?: number;
    active?: boolean;
}
