export interface List {
  eventID: number;
  name: string;
  maxGuests: number;
  price: string;
}
