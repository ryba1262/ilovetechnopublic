export interface MyLists {
    listID: number;
    listName: string;
    price: string;
    eventName: string;
    eventID: number;
    eventDate: Date;
    listToken: string;
}
