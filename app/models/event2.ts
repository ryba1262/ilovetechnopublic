export interface EventID {
  eventID: number;
  name: string;
  placeID: string;
  placeName?: string;
  dateStart: Date;
  placeAddr: string;
  imgURL?: string;
}
