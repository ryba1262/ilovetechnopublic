export interface Places2 {
    cityID?: number;
    cityName?: string;
    cityCountryName?: string;
    placeID?: number;
    placeName?: string;
    placeAdress?: string;
}
