export interface User {
  userID: number;
  login: string;
  active: boolean;
}
