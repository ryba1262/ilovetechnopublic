export interface Event$ {
  eventID?: number;
  name: string;
  placeID: number;
  placeName: string;
  placeLongitude: number;
  placeLatitude: number;
  placeAddress?: string;
  cityID?: number;
  cityName?: string;
  countryID?: number;
  countryName?: string;
  dateTimeStart: Date;
  dateTimeEnd: Date;
  description: string;
  artists: [
    {
      artistID?: number,
      name: string,
      isLive: boolean,
      order: number
    }
  ];
  imgURL?: string;
  links?: [
    {
      link: string,
      order: number,
      type: number
    }
  ];
}
