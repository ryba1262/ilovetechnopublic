export interface Artists {
  artistID?: number;
  name: string;
  isLive: boolean;
  order: 0;
}
