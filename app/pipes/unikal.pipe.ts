import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'unikal'
})
export class UnikalPipe implements PipeTransform {

  transform(value: any, args?: any): any {
      // Remove the duplicate elements
      const art = value.map( x => {
          return x.clinicServices.map(y => {
              return y.value;
          });
      }).reduce((acc, ele, i) => {
          acc = acc.concat(ele);
          return acc;
      });
      return new Set(art);
  }
  }


