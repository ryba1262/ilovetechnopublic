import { Pipe, PipeTransform } from '@angular/core';
import { Artists } from '../models/artists';

@Pipe({
  name: 'sortArtists'
})
export class SortArtistsPipe implements PipeTransform {

  transform(value: Array<Artists>, _args?: any): Array<Artists> {
    return value.sort((a, b) => {
      if (a.order > b.order) {
        return 1;
      } else {
        return -1;
      }
    }
    );
  }

}
