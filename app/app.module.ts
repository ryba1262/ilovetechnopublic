import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchComponent } from './components/search/search.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { HttpService } from './services/http.service';
import { CityService } from './services/city.service';
import {InputTextModule} from 'primeng/inputtext';
import { AdditionService } from './services/addition.service';
import {MegaMenuModule} from 'primeng/megamenu';
import {MenuModule} from 'primeng/menu';
import {PasswordModule} from 'primeng/password';
import { LoginComponent } from './components/login/login.component';
import { TokenInterceptorService } from './services/token-interceptor.service';
import { RegisterComponent } from './components/register/register.component';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './guards/auth.guard';
import { SharedModule } from './shared/shared.module';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { LoaderComponent } from './components/loader/loader.component';
import { LoaderService } from './services/loader.service';
import { ErrorComponent } from './components/error/error.component';
import { ErrorHandlerService } from './services/error-handler.service';
import { MessageService, DialogService } from 'primeng/api';
import {ToastModule} from 'primeng/toast';
import {ConfirmationService} from 'primeng/api';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import { CookieService } from 'ngx-cookie-service';


export function translateHttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './../../assets/i18n/', '.json');
}


@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    LoginComponent,
    RegisterComponent,
    LoaderComponent,
    ErrorComponent,

  ],
  imports: [
    BrowserModule,
    SharedModule,
    BrowserAnimationsModule,
    InputTextModule,
    MegaMenuModule,
    MenuModule,
    PasswordModule,
    ReactiveFormsModule,
    ConfirmDialogModule,
    HttpClientModule,
    ToastModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: translateHttpLoaderFactory,
        deps: [HttpClient]
      },
      isolate: true
    }),
    AppRoutingModule,

  ],
  providers: [HttpService, CityService
    , AdditionService, CookieService
    , AuthService, LoaderService
    , AuthGuard, MessageService
    , ConfirmationService, DialogService
    , { provide: ErrorHandler,
      useClass: ErrorHandlerService},
      {
     provide: HTTP_INTERCEPTORS,
     useClass: TokenInterceptorService,
     multi: true
       }
],
  bootstrap: [AppComponent],
})
export class AppModule { }
