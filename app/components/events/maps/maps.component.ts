import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { icon, latLng, marker, tileLayer, Map } from 'leaflet';
import { CityService } from 'src/app/services/city.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})
export class MapsComponent implements OnInit, OnDestroy {
  map = null;
  zoom = 17;
  gps: [number, number];
  constSubscription: Subscription;
  streetMaps = tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  detectRetina: true,
  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
});

  get options() {
    return {
    layers: [
      this.streetMaps,
      marker(this.gps, {
        icon: icon({
          iconSize: [ 50, 45 ],
          iconAnchor: [ 13, 41 ],
          iconUrl: 'leaflet/hearth.png',
          shadowUrl: 'leaflet/marker-shadow2.png'
        })
      })
    ],
    zoom: 17,
    center: latLng(this.gps)
    };
  }

  constructor(private cityService: CityService, private route: ActivatedRoute) {

   }

  ngOnInit() {
    this.constSubscription = this.cityService.gpsPositionObs.subscribe((tasks: [number, number]) => {
      this.gps = tasks;
          });
  }


  onMapReady(map: Map) {
    this.map = map;
  }
  ngOnDestroy() {
    if (this.constSubscription) {
    this.constSubscription.unsubscribe();
  }}
}
