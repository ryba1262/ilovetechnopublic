import { NgModule } from '@angular/core';
import { EventsComponent, MapsComponent } from './index';
import {ProgressBarModule} from 'primeng/progressbar';
import {DialogModule} from 'primeng/dialog';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { CommonModule } from '@angular/common';
import { EventsRoutingModule } from './events.routing.module';
import { SharedModule } from 'primeng/components/common/shared';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';



@NgModule ({
  declarations: [
    EventsComponent,
    MapsComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    ProgressBarModule,
    DialogModule,
    LeafletModule.forRoot(),
    HttpClientModule,
    TranslateModule,
    EventsRoutingModule,
  ]
})
export class EventsModule {}
