import { NgModule } from '@angular/core';
import { EventsComponent } from './index';
import { Routes, RouterModule } from '@angular/router';

const eventsRouting: Routes = [
  { path: '', component: EventsComponent },
];

@NgModule ({
  imports: [RouterModule.forChild(eventsRouting)],
  exports: [RouterModule]
})
export class  EventsRoutingModule {}
