import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, RouterLinkActive } from '@angular/router';
import { HttpService } from 'src/app/services/http.service';
import { Subscription, Observable } from 'rxjs';
import { CityService } from 'src/app/services/city.service';
import { EventID } from 'src/app/models/event2';
import { Event$ } from 'src/app/models/event';
import { first } from 'rxjs/operators';





@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit, OnDestroy {
  subManager = new Subscription();
  events$: Observable  <Array<EventID>>;
  event: Event$;
  selectedEvent: string;
  display = false;
  image = '../../../assets/images/events//jasna.jpg';
  constructor(private route: ActivatedRoute, private httpService: HttpService, private router: Router, private cityS: CityService) {

  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    const place = this.router.url.slice(1, 70);
    if (this.router.url.slice(1, 7) === 'miasta') {
    this.events$ =  this.httpService.getCityEvent(id, 70).pipe();
    } else {
       if (this.router.url.slice(1, 6) === 'kluby') {
         this.events$ =  this.httpService.getPlaceEvent(id, 70).pipe();
     }
    }
  }

  delay(id: number, name: string) {
    const lat = this.event.placeLatitude;
    const long = this.event.placeLongitude;
    this.cityS.toGPS(lat, long);
    this.selectedEvent = name;
    this.display = true;
  }

  showDialog(id: number, name: string) {
    const sub = this.httpService.getEvent(id)
    .pipe(first())
    .subscribe(
      res => {
        this.event = res;
        this.delay( id, name );
      }
    );
  }

  pokaLink() {
     console.log(this.event);
  }

  pokaEvent() {
    console.log(this.event);
  }

  clearEvent() {
    setTimeout(() => {
      this.selectedEvent = undefined;
      this.event = undefined;
    }, 500
    );
  }

  ngOnDestroy() {
    if (this.subManager) {
      this.subManager.unsubscribe();
    }
  }
}
