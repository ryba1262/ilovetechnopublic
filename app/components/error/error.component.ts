import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { LoaderService } from 'src/app/services/loader.service';



@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit, OnDestroy {

 err: any;
  private subscription: Subscription;
  constructor( private loaderService: LoaderService, private cd: ChangeDetectorRef) { }
  ngOnInit() {
    this.subscription = this.loaderService.errorState.subscribe(
    (e: any) => {
    this.err = e;
    this.cd.detectChanges();
      }
    );
  }
  debag() {
    this.err = undefined;
  console.log(this.err);
  }
  deleteError() {
    this.loaderService.deleteError();
    this.err = undefined;
    console.log(this.err);
    this.cd.detectChanges();
  }
  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
