import { Component, OnInit } from '@angular/core';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-add-list',
  templateUrl: './add-list.component.html',
  styleUrls: ['./add-list.component.css']
})
export class AddListComponent implements OnInit {

listGroup: FormGroup;
  constructor(
      private fb: FormBuilder
    , private httpService: HttpService
    , public ref: DynamicDialogRef
    , public config: DynamicDialogConfig
    ) { }

  ngOnInit() {

    this.listGroup = this.fb.group(
      {
        name: ['', Validators.required],
        maxGuests: ['', Validators.required],
        price: ['', Validators.required],
      }
    );
  }

  get f() { return this.listGroup.controls; }

  sendList() {
    const list = {
      eventID: this.config.data.id,
      name: this.f.name.value,
      maxGuests: this.f.maxGuests.value,
      price: this.f.price.value
     };
     console.log(list);
  }

}
