import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { UserPanelComponent, MyListsComponent } from './index';
import { AdditionComponent } from './index';
import { EditEventComponent } from './editEvents/edit-event/edit-event.component';



const userPanelRouting: Routes = [
  { path: '', component: UserPanelComponent },
  { path: 'addevent', component: AdditionComponent },
  { path: 'editevent', component: EditEventComponent },
  { path: 'mylists', component: MyListsComponent },
  { path: 'myevents', loadChildren: './editEvents/edit-events.module#EditEventsModule' },

];

@NgModule ({
  imports: [RouterModule.forChild(userPanelRouting)],
  exports: [RouterModule]
})
export class  UserPanelRoutingModule {}
