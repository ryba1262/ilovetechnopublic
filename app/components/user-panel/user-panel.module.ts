import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserPanelComponent, AddListComponent,} from './index';
import { UserPanelRoutingModule } from './user-panel.routing.module';
import { EditEventComponent } from './editEvents/edit-event/edit-event.component';
import { AdditionComponent } from './index';
import { AddArtistComponent } from './index';
import {EditorModule} from 'primeng/editor';
import { AddPlaceComponent } from './index';
import { CalendarComponent } from './index';
import {CalendarModule} from 'primeng/calendar';
import { UploadComponent } from './index';
import { EditorComponent } from './index';
import { SharedModule } from 'src/app/shared/shared.module';
import { TabViewModule } from 'primeng/tabview';
import {StepsModule} from 'primeng/steps';
import { TranslateModule } from '@ngx-translate/core';
import { DynamicDialogModule } from 'primeng/components/dynamicdialog/dynamicdialog';
import {SpinnerModule} from 'primeng/spinner';
import { ReactiveFormsModule } from '@angular/forms';
import { MyListsComponent } from './my-lists/my-lists.component';
import {TableModule} from 'primeng/table';
import { UserListComponent } from './my-lists/user-list/user-list.component';
import {CheckboxModule} from 'primeng/checkbox';


@NgModule({
  declarations: [
    UserPanelComponent,
    EditEventComponent,
    AdditionComponent,
    AddArtistComponent,
    AddPlaceComponent,
    CalendarComponent,
    UploadComponent,
    EditorComponent,
    AddListComponent,
    MyListsComponent,
    UserListComponent,
  ],
  entryComponents: [
    AddListComponent,
    UserListComponent
],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    EditorModule,
    CalendarModule,
    SharedModule,
    TabViewModule,
    StepsModule,
    TranslateModule,
    DynamicDialogModule,
    SpinnerModule,
    TableModule,
    CheckboxModule,
    UserPanelRoutingModule
  ],

})

export class UserPanelModule { }
