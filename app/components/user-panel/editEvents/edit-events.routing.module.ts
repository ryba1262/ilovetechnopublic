import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyEventsComponent } from './my-events/my-events.component';


const EditEventsRouting: Routes = [
  { path: '', component: MyEventsComponent },
];

@NgModule ({
  imports: [RouterModule.forChild(EditEventsRouting)],
  exports: [RouterModule]
})
export class  EditEventsRoutingModule {}
