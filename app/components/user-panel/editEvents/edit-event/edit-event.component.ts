import { Component, OnInit, OnDestroy } from '@angular/core';
import { AdditionService } from 'src/app/services/addition.service';

@Component({
  selector: 'app-edit-event',
  templateUrl: './edit-event.component.html',
  styleUrls: ['./edit-event.component.css']
})
export class EditEventComponent implements OnInit, OnDestroy {

  constructor( private addService: AdditionService) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.addService.clearEvent();
  }
}
