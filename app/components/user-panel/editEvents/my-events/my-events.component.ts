import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { MyEvents } from 'src/app/models/myEvents';
import { AdditionService } from 'src/app/services/addition.service';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { DialogService, ConfirmationService } from 'primeng/api';
import { AddListComponent } from '../add-list/add-list.component';







@Component({
  selector: 'app-my-events',
  templateUrl: './my-events.component.html',
  styleUrls: ['./my-events.component.css'],
  providers: [DialogService],
})
export class MyEventsComponent implements OnInit {

  myEvents$: Observable <Array<MyEvents>>;
  cols = [
    { field: 'placeName', header: 'Miejsce', show: true, width: '20%' },
    { field: 'name', header: 'Nazwa eventu', show: true, width: '30%'  },
    { field: 'dateStart', header: 'Data rozpoczęcia', show: true, width: '10%' },
    { field: 'dateEnd', header: 'Data zakończenia', show: true, width: '10%'  },
    { field: 'status', header: 'Status', show: true, width: '15%'  },
    { field: 'draft', header: 'szkic', show: false, width: '0'  },
    { field: 'eventID', header: 'Edytuj', show: true, width: '15%'  },
];
status = [
  { label: 'All', value: null },
  { label: 'Szkic', value: true },
  { label: 'Zatwierdzony', value: 1 },
  { label: 'Opublikowany', value: 2 },
];


  constructor( private http: HttpService
    , private addService: AdditionService
    , private dialogService: DialogService
    , private confirmationService: ConfirmationService ) { }

  ngOnInit() {
    this.myEvents$ = this.http.myEvents().pipe();
  }

  debag(d) {
    this.addService.editEvent(d);
  }
debag2(a) {
  console.log(a);

}


publishEvent(id: number) {
  this.confirmationService.confirm({
      message: 'Czy na pewno chcesz wysłać event?',
      header: 'Publikacja eventu',
      accept: () => {
        this.http.publishEvent(id)
        .pipe(first())
        .subscribe(
          res => {
            this.myEvents$ = this.http.myEvents().pipe();
           }
        );
      }
  });
}

addList(idEvent) {
  this.dialogService.open(AddListComponent, {
    header: 'Dodaj listę',
    width: 'auto',
    contentStyle: {'max-height': '350px', 'overflow': 'auto'},
    data: {
      id: idEvent
  },
});
}
}
