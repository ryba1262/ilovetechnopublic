import { Component, OnInit } from '@angular/core';
import { DynamicDialogRef, DynamicDialogConfig, ConfirmationService } from 'primeng/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpService } from 'src/app/services/http.service';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-list',
  templateUrl: './add-list.component.html',
  styleUrls: ['./add-list.component.css']
})
export class AddListComponent implements OnInit {

public show = true;

listGroup: FormGroup;
  constructor(
      private fb: FormBuilder
    , private httpService: HttpService
    , public ref: DynamicDialogRef
    , public config: DynamicDialogConfig
    , public confirm: ConfirmationService
    , private router: Router
    ) { }

  ngOnInit() {

    this.listGroup = this.fb.group(
      {
        name: ['', Validators.required],
        maxGuests: ['', Validators.required],
        price: ['', Validators.required],
      }
    );
  }

  get f() { return this.listGroup.controls; }

  sendList() {
    const list = {
      eventID: this.config.data.id,
      name: this.f.name.value,
      maxGuests: this.f.maxGuests.value,
      price: this.f.price.value
     };
     this.confirm.confirm(
       {
        message: 'Czy na pewno chcesz wysłać listę?',
        header: 'Publikacja eventu',
        accept: () => {
          this.httpService.createList(list);
          this.ref.close();
        }
       }
     );

  }
}
