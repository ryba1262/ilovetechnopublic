import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditEventsRoutingModule } from './edit-events.routing.module';
import {TableModule} from 'primeng/table';
import {DropdownModule} from 'primeng/dropdown';
import {MultiSelectModule} from 'primeng/multiselect';
import { MyEventsComponent } from './my-events/my-events.component';



@NgModule({
  declarations: [
    MyEventsComponent,
  ],
  imports: [
    CommonModule,
    TableModule,
    DropdownModule,
    MultiSelectModule,
    EditEventsRoutingModule
  ],
})
export class EditEventsModule { }
export class DynamicDialogDemoModule { }
