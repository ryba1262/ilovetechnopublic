import { Component, OnInit, OnDestroy } from '@angular/core';
import { AdditionService } from 'src/app/services/addition.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-addition',
  templateUrl: './addition.component.html',
  styleUrls: ['./addition.component.css']
})
export class AdditionComponent implements OnInit, OnDestroy {

  subManager = new Subscription();
  public currentPage;
  public items = [
    {
      label: 'Miejsce',
    },
    {
      label: 'Data',
    },
    {
      label: 'Tytuł i opis',
    },
    {
      label: 'Lineup',
    },
    {
      label: 'Plakat',
    },
  ];
  constructor(private addService: AdditionService) {

   }

  ngOnInit() {
    const sub = this.addService.currentPageObs.subscribe(t => this.currentPage = t);
    this.subManager.add(sub);
    }


    ngOnDestroy() {
      if (this.subManager) {
        this.subManager.unsubscribe();
      }
    }

}
