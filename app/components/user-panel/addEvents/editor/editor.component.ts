import { Component, OnInit, OnDestroy } from '@angular/core';
import { AdditionService } from 'src/app/services/addition.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit, OnDestroy {

  titleEvent: string;
  description = '';
  subManager = new Subscription();
  constructor(private addService: AdditionService) {

  }

  ngOnInit() {
    const constSubscription = this.addService.eventDescription.subscribe(t => this.description = t);
    const constSubscription2 = this.addService.eventTitle.subscribe(t => this.titleEvent = t);
    this.subManager
      .add(constSubscription)
      .add(constSubscription2);
  }

  addTitle() {
    this.addService.addTitle(this.titleEvent);
  }

  addDescription() {
    this.addService.addDescription(this.description);
  }

  changePage(delta: number) {
    this.addService.changePage(delta);
  }

  ngOnDestroy() {
    if (this.subManager) {
      this.subManager.unsubscribe();
    }
  }
}
