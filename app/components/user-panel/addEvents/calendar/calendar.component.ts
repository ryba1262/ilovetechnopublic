import { Component, OnInit, OnDestroy } from '@angular/core';
import { AdditionService } from 'src/app/services/addition.service';
import { DateEvent } from 'src/app/models/dateEvent';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit, OnDestroy {
  subManager = new Subscription();
  start: Date = null;
  finish: Date = null;
  Data: DateEvent;
  minDate: Date;
  minFinishDate: Date;
  dateFormat = 'yy-mm-dd';
  enDate: any;
  plDate: any;
  constructor(private addService: AdditionService) { }

  ngOnInit() {
    const constSubscription = this.addService.calendar.subscribe(d => this.Data = d);
    if (this.Data !== null) { this.start = this.Data.start, this.finish = this.Data.finish; }
    this.minDate = new Date();
    this.minDate.setDate(this.minDate.getDate() + 1);
    this.minDate.setHours(this.minDate.getHours() + 1);
    this.subManager
      .add(constSubscription);

      this.plDate = {
        firstDayOfWeek: 1,
        dayNames: ["Niedziela", "Poniedziałem", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota"],
        dayNamesShort: ["Nd", "Pon", "Wt", "Śr", "Czw", "Pt", "Sob"],
        dayNamesMin: ["Nd", "Pon", "Wt", "Śr", "Czw", "Pt", "Sob"],
        monthNames: [ "Styczeń","Luty","Marzec","Kwiecień","Maj","Czerwiec","Lipiec","Sierpień","Wrzesień","Październik","Listopad","Grudzień" ],
        monthNamesShort: [ "STY", 'LUT', 'MAR', 'KWI', 'MAJ', 'CZE', 'LIP', 'SIE', 'WRZ', 'PAŹ', 'LIS', 'GRU' ],
        today: 'Dzisiaj',
        clear: 'Wyczyść',
        dateFormat: 'mm/dd/yy'
    };

    this.enDate = {
      firstDayOfWeek: 0,
      dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
      dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
      dayNamesMin: ["Su","Mo","Tu","We","Th","Fr","Sa"],
      monthNames: [ "January","February","March","April","May","June","July","August","September","October","November","December" ],
      monthNamesShort: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
      today: 'Today',
      clear: 'Clear',
      dateFormat: 'mm/dd/yy'
  };
  }

  addCalendar() {
    setTimeout(() => {
      if (this.start) {
        const date = { start: this.start, finish: this.finish };
        this.addService.addCalendar(date);
      }
    }, 60
    );
  }

  removeCalendar() {
    this.addService.removeCalendar();
    this.start = null;
    this.finish = null;
  }

  removeFinishDate() {
    this.addService.removeFinishDate();
    this.finish = null;
  }

  clickDate() {
    const date = { start: this.start, finish: null };
    this.addService.addCalendar(date);
    this.finish = null;
  }

  changeMinDate() {
    this.minFinishDate = this.start;
    this.minFinishDate.setHours(this.minFinishDate.getHours() + 1);
  }


  changePage(delta: number) {
    this.addService.changePage(delta);
  }

  debag2() {
    console.log(this.start, this.finish);
  }

  isFinish() {
    if (this.Data) {
     if (this.Data.finish) {
       return true;
     }
    } else {
      return false;
    }
  }

  ngOnDestroy() {
    if (this.subManager) {
      this.subManager.unsubscribe();
    }
  }

}
