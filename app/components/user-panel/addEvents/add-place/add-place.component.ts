import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { AdditionService } from 'src/app/services/addition.service';
import { Places } from 'src/app/models/places';
import { Subscription, BehaviorSubject } from 'rxjs';
import { RouterLink, ActivatedRoute } from '@angular/router';
import { first, debounceTime, switchMap } from 'rxjs/operators';
import { LoaderService } from 'src/app/services/loader.service';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-add-place',
  templateUrl: './add-place.component.html',
  styleUrls: ['./add-place.component.css']
})
export class AddPlaceComponent implements OnInit, OnDestroy {

  places: Array<Places> = [];
  selectPlace: Places;
  place: any;
  subManager = new Subscription();
  filteredPages: any[];

  searchField: FormControl;
    coolForm: FormGroup;
  constructor(private httpService: HttpService,
    private addService: AdditionService,
    private route: ActivatedRoute,
    private loaderService: LoaderService,
    private fb: FormBuilder) { }

  ngOnInit() {
    const constSubscription = this.addService.placeList.subscribe(e => this.place = e);
    this.subManager.add(constSubscription);

    this.searchField = new FormControl();
    this.coolForm = this.fb.group({search: this.searchField});
  }

  debag() {
    this.addService.ruskiDebag();
  }

  addPlace(): any {
    if (this.selectPlace != null) {
      const constSubscription3 = this.addService.addPlace(this.selectPlace).subscribe(a => this.place = a);
      this.selectPlace = null;
      constSubscription3.unsubscribe();
    }
  }

  time() {
    setTimeout(() => {
      this.addPlace();
    }, 100
    );
  }

  removePlace() {
    this.place = this.place.filter(t => t === null);
    this.addService.removePlace(this.place);
  }

  filterPages(event) {
    const place$ = new BehaviorSubject<string>('');
    place$.next(event.query);
    place$.pipe(
      debounceTime(400),
      switchMap(
        res => {
          return this.httpService.searchPlaces(event.query)
        }
      )
    ).pipe(first())
    .subscribe( res => {
      this.places = res;
        this.filteredPages = this.filterCountry(event.query, this.places);
    } );
    // this.httpService.searchPlaces(event.query).pipe(first())
    // .subscribe(res => {
    //     this.places = res;
    //     this.filteredPages = this.filterCountry(event.query, this.places);
    //   });
  }


  filterCountry(query, countries: Array<Places>): any[] {
    const filtered: any[] = [];
    for (let i = 0; i < countries.length; i++) {
      const a = countries[i];
      if (a.name.toLowerCase().indexOf(query.toLowerCase()) === 0) {
        filtered.push(a);
      }
    }
    return filtered;
  }

  noLoader() {
    this.loaderService.toggleShow(false);
  }

  loader() {
    this.loaderService.toggleShow(true);
  }

  changePage(delta: number) {
    this.addService.changePage(delta);
  }

  ngOnDestroy() {
    if (this.subManager) {
      this.subManager.unsubscribe();
    }
  }
}
