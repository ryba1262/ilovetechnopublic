import { Component, OnInit, OnDestroy } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AdditionService } from 'src/app/services/addition.service';
import { Subscription } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit, OnDestroy {
  base64textString = '';
  img: string;
  selectedFile = null;
  link: string;
  subManager = new Subscription();
  constructor(public _DomSanitizer: DomSanitizer
    , private addService: AdditionService
    , private httpService: HttpService
    , private router: Router
    , private confirmationService: ConfirmationService) {
  }

  ngOnInit() {
    this.link = this.router.url;
    const constSubscription = this.addService.imgBase64.subscribe(i => this.selectedFile = i);
    const constSubscription2 = this.addService.imgURL.subscribe(i => this.img = i);
    this.subManager
      .add(constSubscription)
      .add(constSubscription2);
  }

  onFileSelected(event) {
    const file = event.target.files[0];
    if ((file.type !== 'image/jpeg') && (file.type !== 'image/png') && (file.type !== 'image/gif')) {
      throw Error('Nieoprawny format!');
    } else if (file.size > 500000) {
      throw Error('Maksymalny rozmiar zdjęcia to 500 kb');
    } else {
      this.addService.addImage(file);
    }
  }

  removeUp() {
    this.addService.removeImage();
  }

  changePage(delta: number) {
    this.addService.changePage(delta);
  }

  sendEvent() {
      this.addService.sendEvent();
  }

  confirm() {
    this.confirmationService.confirm({
        message: 'Czy na pewno chcesz wysłać event?',
        accept: () => {
          this.sendEvent();
        }
    });
}


  ngOnDestroy() {
    if (this.subManager) {
      this.subManager.unsubscribe();
    }
  }
}
