import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { Subscription } from 'rxjs';
import { Artists } from 'src/app/models/artists';
import { AdditionService } from 'src/app/services/addition.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Links } from 'src/app/models/links';
import { urls } from 'src/app/models/urls';
import { first } from 'rxjs/operators';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'app-add-artist',
  templateUrl: './add-artist.component.html',
  styleUrls: ['./add-artist.component.css']
})
export class AddArtistComponent implements OnInit, OnDestroy {
  artists: any[] = [];
  selectArtist: any;
  art: string;
  artList: Array<Artists> = [];
  subManager = new Subscription();
  filteredPages: any[];
  selectLink: string;
  links: Links[];

  constructor(private httpService: HttpService
    , private addService: AdditionService
    , public sanitizer: DomSanitizer
    , private loaderService: LoaderService) {

   }

  ngOnInit() {
    const constSubscription2 = this.addService.artistsList.subscribe(art => this.artList = art);
    const constSubscription4 = this.addService.linkList.subscribe(art => this.links = art);
    this.subManager
    .add(constSubscription2)
    .add(constSubscription4);
  }
  addArtist() {
    if (this.selectArtist != null) {
      const constSubscription3 = this.addService.addArtist(this.selectArtist).subscribe(t => this.artList = t);
    this.selectArtist = '';
    // console.log(this.artList);
    constSubscription3.unsubscribe();
  }}

  time() {
    setTimeout(() => {
      this.addArtist();
    }, 100
    );
  }
addLink() {

  if (this.selectLink.slice(0, 17) === urls.youtube) {
    const ytLink = this.selectLink.substr(17);
    this.addService.addLink(ytLink, 1);
    this.selectLink = '';
  } else if ( this.selectLink.slice(0, 161) === urls.soundCloud) {
    console.log(this.selectLink.slice(0, 161));
    const pos1 = this.selectLink.indexOf('tracks/') + 7;
    const pos2 = this.selectLink.indexOf('&color');
    const soundLink = this.selectLink.slice(pos1, pos2);
    this.addService.addLink(soundLink, 2);
    this.selectLink = '';
  } else {
    throw Error('Niepoprawny url!');
  }

}
debag() {
  const pos1 = urls.soundCloud2.indexOf('tracks/') + 7;
    const pos2 = urls.soundCloud2.indexOf('&color');
    const soundLink = urls.soundCloud2.slice(0, 161);
  console.log(pos1);
    console.log(pos2);
  console.log(soundLink);
}
removeLink(link: Links) {
  const links = this.links.filter( e => e !== link);
  this.addService.removeLink(links);
}

  disableSaveButton() {
    if (this.selectArtist == null || this.selectArtist === '' || this.selectArtist === ' ') {
      return true;
    }
    return false;
  }
  removeArt(art: Artists) {
    this.artList = this.artList.filter( e => e !== art);
    this.addService.removeArt(this.artList);
  }
  isLiveChange(art: Artists) {
    this.artList = this.artList.filter( e => e !== art);
    if (art.isLive === false) {
      const a = { artistID: art.artistID, name: art.name, order: art.order, isLive: true };
      this.artList.push(a);
     } else {
      const a = { artistID: art.artistID, name: art.name, order: art.order, isLive: false };
      this.artList.push(a);
     }
     this.addService.removeArt(this.artList);
  }

  filterPages(event: any) {
    this.httpService.searchArtists(event.query)
    .pipe(first())
    .subscribe(
      res => {
        this.artists = res;
        this.filterDelay(event);
      }
    );

  }

  filterDelay(event: any) {
    this.filteredPages = this.filterCountry(event.query, this.artists);
  }


  filterCountry(query, countries: Array<Artists>): any[] {
    const filtered: any[] = [];
    for (let i = 0; i < countries.length; i++) {
      const a = countries[i];
      if (a.name.toLowerCase().indexOf(query.toLowerCase()) === 0) {
        filtered.push(a);
      }
    }
    return filtered;
  }

  noLoader() {
    this.loaderService.toggleShow(false);
  }

  loader() {
    this.loaderService.toggleShow(true);
  }

  changePage(delta: number) {
    this.addService.changePage(delta);
}

ngOnDestroy() {
 if (this.subManager) {
   this.subManager.unsubscribe();
  }
}
}
