import { Component, OnInit } from '@angular/core';
import { MyLists } from 'src/app/models/myLists';
import { Observable } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';
import { MessageService, DialogService } from 'primeng/api';
import { UserListComponent } from './user-list/user-list.component';

@Component({
  selector: 'app-my-lists',
  templateUrl: './my-lists.component.html',
  styleUrls: ['./my-lists.component.css'],
  providers: [DialogService],
})
export class MyListsComponent implements OnInit {
  rowGroupMetadata: any;
  myLists$: Observable <Array<MyLists>>;
  lists: MyLists[];
  cols = [
  { field: 'listName', header: 'Nazwa listy', show: true, width: '20%' },
  { field: 'eventName', header: 'Nazwa eventu', show: true, width: '20%' },
  { field: 'eventDate', header: 'Data eventu', show: true, width: '20%' },
  { field: 'price', header: 'Zniżka', show: true, width: '20%' },
  { field: 'listID', header: 'Operacje', show: true, width: '20%' },
  { field: 'eventID', header: 'Operacje2', show: false, width: '0%' },
  { field: 'listToken', header: 'Operacje3', show: false, width: '0%' },

];



  constructor(
    private http: HttpService
    , private messageService: MessageService
    , private dialogService: DialogService
  ) { }

  ngOnInit() {

   const constSubscription = this.http.myLists().subscribe(
      l => {
        this.lists = l;
        this.updateRowGroupMetaData();
      }
     );

  }

  onSort() {
    this.updateRowGroupMetaData();
}

debag2(val) {
  console.log(val);
  const selBox = document.createElement('textarea');
  selBox.style.position = 'fixed';
  selBox.style.left = '0';
  selBox.style.top = '0';
  selBox.style.opacity = '0';
  selBox.value = 'link:' + val + '.com';
  document.body.appendChild(selBox);
  selBox.focus();
  selBox.select();
  document.execCommand('copy');
  document.body.removeChild(selBox);
  this.messageService.add({ severity: 'success', summary: 'Token', detail: 'Link został skopiowany do schowka' });
}

userList(idEvent) {
  this.dialogService.open(UserListComponent, {
    header: 'Dodaj usera',
    width: 'auto',
    contentStyle: {'height': 'auto', 'overflow': 'auto'},
    data: {
      id: idEvent
  },
});
}

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    if (this.lists) {
        for (let i = 0; i < this.lists.length; i++) {
            const rowData = this.lists[i];
            const eventName = rowData.eventName;
            if (i === 0) {
                this.rowGroupMetadata[eventName] = { index: 0, size: 1 };
            } else {
                const previousRowData = this.lists[i - 1];
                const previousRowGroup = previousRowData.eventName;
                if (eventName === previousRowGroup) {
                    this.rowGroupMetadata[eventName].size++;
                } else {
                    this.rowGroupMetadata[eventName] = { index: i, size: 1 };
                }
            }
        }
    }
}


}
