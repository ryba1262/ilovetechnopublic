import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, MessageService } from 'primeng/api';
import { HttpService } from 'src/app/services/http.service';
import { first, debounceTime, switchMap } from 'rxjs/operators';
import { User } from 'src/app/models/user';
import { LoaderService } from 'src/app/services/loader.service';
import { BehaviorSubject, Observable, Subject } from 'rxjs';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  filteredUsers: any[];
  selectUsers: Array<User> = [];
  selectUser: User;
  eventID: number;
  users: User[] = [];
  selectedValues: string[] = [];
  selectedRemove: number[] = [];
  constructor(public config: DynamicDialogConfig,
              private httpService: HttpService,
              private loaderService: LoaderService,
              public messageService: MessageService
    ) { }

  ngOnInit() {
      this.eventID = this.config.data.id;
      this.getUsers();
    }

    getUsers(): Observable<any> {
      const obs = new Subject();
        setTimeout(() =>
        this.httpService.getEventUsers(this.eventID).subscribe(
          users => {

            this.users = users;
            obs.next(users);
          }
        )
        );
        return obs;
    }

  filterPages(event) {
    const place$ = new BehaviorSubject<string>('');
    place$.next(event.query);
    place$.pipe(
      debounceTime(400),
      switchMap(
        res => {
          const user = {login: event.query};
          return this.httpService.findUser(user);
        }
      )
    ).pipe(first())
    .subscribe( res => {
      this.selectUsers = res;
        this.filteredUsers = this.filterCountry(event.query, this.selectUsers);
    } );

    // const user = {login: event.query };
    // this.httpService.findUser(user).pipe(first())
    // .subscribe(res => {
    //     console.log(res);
    //     this.selectUsers = res;
    //     this.filteredselectUsers = this.filterCountry(event.query, this.selectUsers);
    //   });
  }


  filterCountry(query, countries: Array<User>): any[] {
    const filtered: any[] = [];
    for (let i = 0; i < countries.length; i++) {
      const a = countries[i];
      if (a.login.toLowerCase().indexOf(query.toLowerCase()) === 0) {
        filtered.push(a);
      }
    }
    return filtered;
  }
  noLoader() {
    this.loaderService.toggleShow(false);
  }

  loader() {
    this.loaderService.toggleShow(true);
  }

  addUserList(user: User) {
      const check = this.users.find(x => x.login === user.login);
      if (!check) {
        const user2 = { userID: user.userID, login: user.login, active: false };
        this.users.push(user2);
      }
   }

   removeUserList() {
    console.log('1: ', this.users);
     console.log('2: ', this.selectedValues);
     const afterRemove = this.users.filter( user => !this.selectedValues.includes(user.login));
     const remove = this.users.filter( user => this.selectedValues.includes(user.login));
     this.users = afterRemove;
     this.selectedRemove = remove.map( users => users.userID );
     console.log('3: ', this.users);
     console.log('4: ', this.selectedRemove);
   }

  debag() {
    let reqRemove: number = this.selectedRemove.length;
    let additionUsers: number[] = [];
    additionUsers = this.users.filter( user => user.active === false).map( send => send.userID );

    if (this.selectedRemove.length > 0) {
      // console.log('cały: ', this.selectedRemove)
      // console.log('pojedynczy: ', this.selectedRemove[1]);
      for (let i = 0; i < this.selectedRemove.length; i++ ) {
        const index = this.selectedRemove[i];
        console.log('res');
        this.httpService.removeUserList(this.eventID, index)
        .pipe(first()).subscribe(
          res => {
            reqRemove--;
            if (reqRemove === 0 && additionUsers.length === 0) {
            this.getUsers()
            .subscribe(
              () => {
                this.selectedRemove = [];
                this.messageService.add({ severity: 'success', summary: 'Sukces', detail: 'Lista została zaktualizowana' });
              }
            );
            } else if (reqRemove === 0 && additionUsers.length > 0) {
              this.sendUsers(additionUsers);
            }
            // console.log(req);
          }
        );
      }
    } else if (this.selectedRemove.length === 0 &&  additionUsers.length > 0) {
        this.sendUsers(additionUsers);
    }
    }

    sendUsers(users: number[]) {
      let reqAdd: number = users.length;
      for (let i = 0; i < reqAdd; i++) {
        const index = users[i];
        this.httpService.addUserToList(this.eventID, index)
        .pipe(first()).subscribe(
          () => {
            reqAdd--;
            if (reqAdd === 0) {
              this.messageService.add({ severity: 'success', summary: 'Sukces', detail: 'Lista użytkowników została zaktualizowana' });
            }
          }
        );
      }
    }
  }



