import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  returnUrl: string;
  error = '';

constructor(private _auth: AuthService,
  private formBuilder: FormBuilder,
  private route: ActivatedRoute,
  private router: Router) { }

ngOnInit() {
  this.loginForm = this.formBuilder.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
});
}

get f() { return this.loginForm.controls; }

onSubmit() {
  this.submitted = true;

  // stop here if form is invalid
  if (this.loginForm.invalid) {
      return;
  }

  // this.loading = true;
  this._auth.loginUser(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
          res => {
            this._auth.logIn(res);
            console.log(res);
            this.router.navigate(['/user']);
          },
        );
}

goRegister() {
  this.router.navigate(['/register']);
}

isAuth() {
  return this._auth.getToken();
}

}
