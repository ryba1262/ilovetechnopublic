import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from 'src/app/services/http.service';
import { Places2 } from 'src/app/models/places2';
import { Subscription } from 'rxjs';
import { LoaderService } from 'src/app/services/loader.service';
import { first } from 'rxjs/operators';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit, OnDestroy {

  city: string;
  subManager = new Subscription();
  list: Array<Places2> = [];
  filteredPages: any[];

  constructor(private httpService: HttpService, private router: Router, private loaderService: LoaderService) { }

  ngOnInit() {
  }


  filterPages(event) {
    this.httpService.searchCityOrPlace(event.query)
    .pipe(first())
    .subscribe(
      res => {
        this.list = res;
        this.filterDelay(event);
      }
    );
  }

  filterDelay(event) {
    const d = this.list.filter(aa => aa.cityName !== null).map(p => {
      const list = { cityID: p.cityID, cityName: p.cityName, placeID: p.placeID, placeName: p.cityName };
      return list;
    });
    const e = this.list.filter(bb => bb.placeName !== null).map(p => {
      const list = { cityID: p.cityID, cityName: p.placeName, placeID: p.placeID, placeName: p.placeName };
      return list;
    }).concat(d);
    this.filteredPages = this.filterp(event.query, e);
  }

  filterp(query, countries: Array<Places2>): any[] {
    const filtered: any[] = [];
    for (let i = 0; i < countries.length; i++) {
      const country = countries[i];
      if (country.cityName.toLowerCase().indexOf(query.toLowerCase()) === 0) {
        filtered.push(country);
      }
    }
    return filtered;
  }

  gotoDetail(place: Places2): void {
    if (place.cityID != null) {
      const link = ['/miasta', place.cityID];
      this.router.navigate(link);
    } else {
      const link = ['/kluby', place.placeID];
      this.router.navigate(link);
    }
  }

  clearInput() {
    this.list = [];
  }

  noLoader() {
    this.loaderService.toggleShow(false);
  }

  loader() {
    this.loaderService.toggleShow(true);
  }

  ngOnDestroy() {
    if (this.subManager) {
      this.subManager.unsubscribe();
    }
  }

}
