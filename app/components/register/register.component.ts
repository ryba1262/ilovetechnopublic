import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';

@Component ({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(private _auth: AuthService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router) { }


    ngOnInit() {
      this.loginForm = this.formBuilder.group({
        login: ['', Validators.required],
        password: ['', Validators.required],
        email: ['', Validators.required]
    });
    }

    get f() { return this.loginForm.controls; }

    onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.loginForm.invalid) {
          return;
      }

      this.loading = true;
      this._auth.registerUser(this.f.login.value, this.f.password.value, this.f.email.value)
          .pipe(first())
          .subscribe(
               res => {
                 console.log(res);
                this.router.navigate(['/login']);
               },
            );
  }
    isAuth() {
      return this._auth.getToken();
    }


}
